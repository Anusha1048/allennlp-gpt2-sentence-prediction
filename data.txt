we are probably already extinct hikes
the word on this street is ominous so
make a point about statistics if you
that's ever lived at any point in
Earth's history your odds of picking one
that's already extinct would be better
than ninety-nine out of a hundred so if
you knew nothing else about the species
Homo sapiens your safest bet would be
that it's an extinct species you'd lose
that bet but for the vast vast majority
of species throughout life's history
extinction is something all too real as
extinction is one of the most important
elements of life's history but as a
philosopher I have to ask what is
extinction there's a standard answer to
that question but that standard answer
can be kind of misleading and we need to
change that because extinction is more
than just an academic problem at this
very moment as the continent of
Australia burns we stand to lose
hundreds of species within the next few
weeks and it's not going to end there by
the end of this century something
between 30 and 50 percent of all species
alive on this planet right now will go
extinct when that happens there will be
no announcement no signal they'll just
be gone and we won't know until it's
already too late to do anything about it
something like that happens for this
that tiger you caught by the toe in the
according to one legend pinching the
Tasmanian tiger by a toe on its hind
limb would render the animal utterly
helpless and fit for capture so it's no
it's one of those moments in history
that people always remember for my
were when Kennedy was shot and for my
generation it was where you were when
 black president and he gave a speech
that was about hope more than it was
about winning a presidential race
regardless of your politics it's a
monumental day in our history that day
was 11 years one month and three days
ago I remember that day not for politics
 County Sheriff's Department came to my
home they knocked on the door they came
in and they told me I was under arrest
the youngest of my four boys was home at
the time he was three years old I asked
the deputies to let me call someone to
Pets and he had all his stuffed animals
in a circle around him when Child
Protective Services came to take him
comfort but I could not comfort him
had this infectious curiosity that's
really typical in most five-year-olds
but from that point forward I started to
see a fire in her that I'd not quite
empowered her to lean into that natural
curiosity it was a place that she had
access to every day that not only
allowed her to find purpose in her
learning but encouraged her to do so now
as a teacher this is a pretty big moment
for me I mean look at my kindergarten
daughter taking full advantage of a free
education I mean she must be a teacher's
kid right but the more I got to know her
there's actually a trait that most of
her classmates shared these
kindergarteners love of school was so
imperfect life made her who and what she
is today it's the story of a woman who
in pursuit of her dreams and aspirations
made other people realize that if you
think that your life is hard and you're
giving up on that because you think your
life is unfair think again because when
woman who made people realize that
sometimes problems are not too big we
are too small because we cannot handle
 those incidents break you deform you but
they mold you into the best version of
you and the same thing happened to me
and I'm going to share what exactly
for the very first time on an
international level I was 18 years old
when I got married I belonged to a very
we're good daughters never say no to
their parents my father wanted me to get
and all I said was if that makes you
happy I'll say yes and of course it was
never a happy marriage just about after
years ago I made a car accident
somehow my husband fell asleep and the
car fell in the ditch he managed to jump
out saved himself I'm happy for him but
I stayed inside the car and I sustained
a lot of injuries the list is a bit long
don't get scared