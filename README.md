AllennlpframeworkOpen-AI GPT-2language model to generate sentences:-

1.Language Modelling (LM)is one of the most important tasks of modern Natural Language Processing (NLP). A language model is a probabilistic model which predicts the next word or character in a document.

2.GPT-2 is a successor of GPT, the original NLP framework by OpenAI. The full GPT-2 model has 1.5 billion parameters, which is almost 10 times the parameters of GPT.

3.The pre-trained model contains data from 8 million web pages collected from outbound links from Reddit.

4.understand how GPT-2 works(GPT-2 architecture)https://towardsdatascience.com/one-language-model-to-rule-them-all-26f802c906604.Along with BERT, GPT-2 has been making waves in the NLP world.

5.In the AllenNLP framework, a Predictoris used to wrap the model for prediction.

6.The predictor can make predictions via the predict_jsonmethod, which takes in a dictionary of the form {"previous": "some text to predict"}.

7.To generate a full sentence with an input text the most likely generate next word up to until we found “.”, “?”, “!”.

8.Generate top 9 sentences with the input text.

9.When computing sentence probability,  we need to prepend the sentence with a dummy start token (e.g. <|endoftext|>) to get the full sentence probability/perplexity?  11.the sentence which has lower perplexity that is better sentence

