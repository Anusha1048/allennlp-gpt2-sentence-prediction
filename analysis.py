import string
import copy
import gpt2
import math
from allennlp.predictors import Predictor
from pytorch_pretrained_bert.tokenization_gpt2 import GPT2Tokenizer
from pytorch_pretrained_bert.modeling_gpt2 import GPT2LMHeadModel
import torch
from collections import defaultdict
predictor = gpt2.Gpt2Predictor()
model = GPT2LMHeadModel.from_pretrained('https://storage.googleapis.com/allennlp/models/gpt2-345M-dump')
model.eval()
tokenizer = GPT2Tokenizer.from_pretrained("gpt2")


def predict_top3(input_str):
    if not input_str:
        raise Exception('input string required')
    output_str = copy.deepcopy(input_str)
    prediction = predictor.predict_json({"previous": output_str})
    return prediction["words"]


def predict_until_punctuation(input_str):
    if not input_str:
        raise Exception('input string required')
    output_str = copy.deepcopy(input_str)
    while len(output_str) != 0 and output_str[-1] not in [".", "!", "?"] and output_str[-2] not in [".", "!", "?"]:
        prediction = predictor.predict_json({"previous": output_str})
        output_str += prediction["words"][0]
    return output_str


def score(sentence):
    with torch.no_grad():
        # print('Entered')
        tokenize_input = tokenizer.tokenize(sentence)
        tensor_input = torch.tensor([[50256] + tokenizer.convert_tokens_to_ids(tokenize_input)])
        loss = model(tensor_input, lm_labels=tensor_input)
        return loss.item()


if __name__ == "__main__":
    f = open('data.txt')
    Lines = f.readlines()
    for line in Lines:
        # print("Next Sentence in Doc")
        top9_sentences = []
        input_text = line.lower().strip()
        # print(input_text)
        top3_list = predict_top3(input_text)
        # print("Top3 Words After input")
        # print(top3_list)
        for word in top3_list:
            output_text = input_text + word
            next_top3 = predict_top3(output_text)
            # print("Top3 words For Each word in top3")
            # print(next_top3)
            for word in next_top3:
                top9_sentences.append(output_text + word)
        # print(top9_sentences)
        top_final_sentences = []
        for sent in top9_sentences:
            final_sent = predict_until_punctuation(sent)
            # print(final_sent)
            if (final_sent[-2]) in [".", "!", "?"]:
                top_final_sentences.append(final_sent[0:-1])
            else:
                top_final_sentences.append(final_sent)
        # print(top_final_sentences)
        loss_score_dict = {}
        for sent in top_final_sentences:
            loss_score_dict[sent] = score(sent)
        # print("Top 9 Sentences with their perflexity")
        # for sentence, sc in loss_score_dict.items():
            # print(sentence, sc)
        # print("Sentence which has lowest  perflexity:-")
        min_loss = min(loss_score_dict.values())
        for key in loss_score_dict:
            if loss_score_dict[key] == min_loss:
                print(key)
                break
