import math
from allennlp.predictors import Predictor
from pytorch_pretrained_bert.tokenization_gpt2 import GPT2Tokenizer
from pytorch_pretrained_bert.modeling_gpt2 import GPT2LMHeadModel
import torch
import gpt2

model = GPT2LMHeadModel.from_pretrained('https://storage.googleapis.com/allennlp/models/gpt2-345M-dump')
model.eval()
def calc_prob(model, text):
    tokenizer = GPT2Tokenizer.from_pretrained("gpt2")
    texts = ['xbos xfld 1 ' + text]
    tokens = Tokenizer.proc_all_mp(partition_by_cores(texts))

    # initilize probability to 1
    prob = 1.0
    aggregated_token_indexes = []

    # we are only dealing with one sentence, so only look at tokens[0]
    for token in tokens[0]:

        # get the index of the token
        token_idx = stoi[token]

        # don't predict on a zero-length array
        if len(aggregated_token_indexes) > 0:
            # we want a [x,1] array where x is the number
            #  of words inputted (including the prefix tokens)
            ary = np.reshape(np.array(aggregated_token_indexes), (-1, 1))

            # turn this array into a tensor
            tensor = torch.from_numpy(ary)

            # wrap in a torch Variable
            variable = Variable(tensor)

            # batch size of 1
            m[0].bs = 1

            # make sure we are in evaluation mode
            m.eval()
            m.reset()

            # predict what word comes next, based on the text BEFORE this current word
            res, *_ = m(variable)

            # res[-1] contains the scores of each possible token in the vocabuary
            # use softmax to turn it into a probability
            all_token_probs = F.softmax(res[-1]).data

            # find the probability of this token and multiply by the probability of all the previous text
            prob *= all_token_probs[token_idx]

        # aggrgate this token on for the next loop
        aggregated_token_indexes.append(token_idx)

    return prob
text="What is the fastest car in the world?"
probability=calc_prob(model,text)
print(probability)