
import string
import copy
import gpt2

predictor = gpt2.Gpt2Predictor()

def predict_until_punctuation(input_str):
  if not input_str:
    raise Exception('input string required')
  output_str = copy.deepcopy(input_str)
  while len(output_str) != 0 and output_str[-1] not in [".", "!", "?"] and output_str[-2] not in [".","!","?"]:
    prediction = predictor.predict_json({"previous": output_str})
    # print("p")
    # print(prediction)
    # print("o")
    # print(output_str)
    print(output_str)
    output_str += prediction["words"][1]
    # print("o1")
    # print(output_str)

  return output_str


scores=[]
if __name__ == "__main__":
    f = open('total_sentences.txt')
    Lines = f.readlines()
    for line in Lines:
        text = predict_until_punctuation(line.lower().strip())
        print(text)
        get_prob=predictor.predict_json1({"previous":text})
        probability = get_prob["probabilities"]
        print(probability.sum())
