# import spacy
# from spacy_langdetect import LanguageDetector
# nlp = spacy.load('en')
# nlp.add_pipe(LanguageDetector(), name='language_detector', last=True)
# prob_dict={}
# f = open('sdata.txt')
# Lines = f.readlines()
# sent_list=[]
# values_list=[]
# for line in Lines:
#    doc = nlp(line.strip())
#    # document level language detection. Think of it like average language of the document!
#    # print(doc._.language)
#    # sentence level language detection
#    probability=doc._.language['score']
#    sent_list.append(doc)
#    values_list.append(probability)
#    print(doc,probability)
# print(sent_list)
# print(values_list)
# maxpos = values_list.index(max(values_list))
# print(maxpos)
# # for key,val in prob_dict.items():
# #    print(key,val)
# # max_prob = max(prob_dict.values())
# # for key in prob_dict:
# #    if prob_dict[key] == max_prob:
# #       print(key)
# #       break

import spacy
from spacy_langdetect import LanguageDetector
nlp = spacy.load('en')
nlp.add_pipe(LanguageDetector(), name='language_detector', last=True)
text = 'This is an english text.'
doc = nlp(text)
# document level language detection. Think of it like average language of the document!
print(doc._.language)
# sentence level language detection
for sent in doc.sents:
   print(sent, sent._.language)