import math
from allennlp.predictors import Predictor
from pytorch_pretrained_bert.tokenization_gpt2 import GPT2Tokenizer
from pytorch_pretrained_bert.modeling_gpt2 import GPT2LMHeadModel
import torch

model = GPT2LMHeadModel.from_pretrained('https://storage.googleapis.com/allennlp/models/gpt2-345M-dump')
model.eval()
tokenizer = GPT2Tokenizer.from_pretrained("gpt2")


# def calc_prob(model, text):
#     texts = ['xbos xfld 1 ' + text]
#     tokens = Tokenizer.proc_all_mp(partition_by_cores(texts))
#
#     # initilize probability to 1
#     prob = 1.0
#     aggregated_token_indexes = []
#
#     # we are only dealing with one sentence, so only look at tokens[0]
#     for token in tokens[0]:
#
#         # get the index of the token
#         token_idx = stoi[token]
#
#         # don't predict on a zero-length array
#         if len(aggregated_token_indexes) > 0:
#             # we want a [x,1] array where x is the number
#             #  of words inputted (including the prefix tokens)
#             ary = np.reshape(np.array(aggregated_token_indexes), (-1, 1))
#
#             # turn this array into a tensor
#             tensor = torch.from_numpy(ary)
#
#             # wrap in a torch Variable
#             variable = Variable(tensor)
#
#             # batch size of 1
#             m[0].bs = 1
#
#             # make sure we are in evaluation mode
#             m.eval()
#             m.reset()
#
#             # predict what word comes next, based on the text BEFORE this current word
#             res, *_ = m(variable)
#
#             # res[-1] contains the scores of each possible token in the vocabuary
#             # use softmax to turn it into a probability
#             all_token_probs = F.softmax(res[-1]).data
#
#             # find the probability of this token and multiply by the probability of all the previous text
#             prob *= all_token_probs[token_idx]
#
#         # aggrgate this token on for the next loop
#         aggregated_token_indexes.append(token_idx)
#
#     return prob
def score(sentence):
    with torch.no_grad():
        tokenize_input = tokenizer.tokenize(sentence)
        tensor_input = torch.tensor([ [50256]  +  tokenizer.convert_tokens_to_ids(tokenize_input)])
        loss = model(tensor_input, lm_labels=tensor_input)
        print(loss.item())


# sentences=['what is the fastest car in the earth?','what is the fastest car in the world?','what is the fastest car on earth?','what is the fastest car you ever driven?','hope you are well.','hope you are doing okay.','hope you are enjoying this.','hope you are doing well.','hope you are doing okay.','hope you are doing fine, but if it was not possible, we are not able, we are unable.','i bought a watch from a local store and it was a very nice watch.','i bought a watch, a Role, a watch that I tought would last forever, a Role that was so expensive I was willing, if it wasn´t worth the price of the watches I bought it for.','i bought a watch for $200 and had a watch made.','you will know exactly what I\'m talking about when you see the results of your own experiments.','you will know exactly what I\'m talking to ya, I\'ll tell you when to expect the call.','you will know exactly what I\'m talking of).','I was 18 years old when I first heard about the movie.','I was 18 years old when I started writing, but it wasn´t long until my first book, "A Song for Myself" (I wrote the book before the internet was invented).','I was 18 years old when I was raped and I didn`ve been told by the doctors to go back and see my mother, my brother.','I belong to a very conservative family, and I\'m not going to be bullied into voting for a candidate who is not conservative.','I belong to a very conservative family, but we\'re very proud to have our daughter here.','I belong to a very conservative family, so it is hard not being a conservative.','I feel like I\'ve been blessed more than anything in my life," he said.','I feel like I\'ve been blessed more than I have had the misfortune to suffer," she says, her eyes still wide with tears, her face contorting into an angry frown as her words are interrupted.','I feel like I\'ve been blessed more than anyone in this whole process.','when are you going back to work?','i have already mentioned it is gratitude that makes the difference between success or not, but I\'m going with gratitude for the fact I have been successful in this endeavor, not for my ability.','when are you going back to work?'" he asked, adding, "'You know what, you can go to the hospital, you know, get checked up, and you\'re going back home.','when are you going back to work?','that\'s why i have to say that i\'m not a fan of the new version of the game.','that\'s why i have to ask.','that\'s why i have to be honest and admit I don` t have the same level or skills.','i finally ended up in the hospital.','i finally ended up with the best possible result, but he still lost to a guy named Juri who had been in his corner since he first came out of highschool, so it\'s hard not be impressed by him, too, even though his opponent is probably a lot worse than Jori, and even if you don`re into that sorta stuff.','i finally ended up at his house in New Orleans and started working as the owner.','i have already mentioned it is gratitude.','i have already mentioned it is gratitude that makes the difference between success or not, but I\'m going with gratitude for the fact I have been successful in this endeavor, not for my ability.','i have already mentioned it is gratitude, but also love and kindness and respect and kindness to the person that we have lost.','i have already mentioned it is gratitude that is the key to success.','i have already mentioned it is gratitude that makes the difference between success or not, but I\'m going with gratitude for the fact I have been successful in this endeavor, not for my ability.','i have already mentioned it is gratitude that keeps them coming to our church and our community, not just because we have been there and helped with their struggles.','i have already mentioned it is gratitude that makes the difference between success or not, but I\'m going with gratitude for the fact I have been successful in this endeavor, not for my ability.','you will know exactly what I\'m talking about when you see the results of your own experiments.','you will know exactly what I\'m talking about) and I\'ll tell them to get back in line and wait.','you will know exactly what I\'m talking about)','i feel like i\'ve been blessed more than i\'ve been cursed.','i feel like i\'ve been blessed more than i have ever felt before, i\'m not even close.','i feel like i\'ve been blessed more than i thought possible.','that\'s why i have to say that i\'m not a fan of the new version of the game.','that\'s why i have to say this: i am a big believer that you can make your life a lot easier if your goals aren´re aligned.','that\'s why i have to say it, but the fact remains: I don`T want this','i have already mentioned it is gratitude that keeps us going.','i have already mentioned it is gratitude that keeps me coming to this blog, but it\'s not the same thing.','i have already mentioned it is gratitude that keeps them coming to our church and our community, not just because we have been there and helped with their struggles.','i have already mentioned it is gratitude that makes us happy.','i have already mentioned it is gratitude that makes the difference between success or not, but I\'m going with gratitude for the fact I have been successful in this endeavor, not for my ability.','i have already mentioned it is gratitude that makes you a man and that you can live a good, honest and fulfilling lifestyle.','i have already mentioned it is gratitude that is the key to success.','i have already mentioned it is gratitude that is most powerful in our life, but we should not be so easily distracted from our true nature by our desire to be happy and happy is what is needed to achieve this goal, we must first of course realize the importance and value that we have to our life.','i have already mentioned it is gratitude that is needed for this gift, for this blessing of life and the blessing to come.']
 # lower value is the best sentence.
loss_score={}
for sent in sentences:
    loss_score[sent]=score(sent)
print(loss_score)
# output=calc_prob(model,'what is the fastest car in the world?')
# model = LanguageModel('https://storage.googleapis.com/allennlp/models/gpt2-345M-dump')
# p1 = model.perplexity('This is a well constructed sentence')
# p2 = model.perplexity('Bunny lamp robert junior pancake')
# print(p1,p2)