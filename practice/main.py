# import gpt2
# predictor = gpt2.Gpt2Predictor()
# result = predictor.predict_json({"previous": "Toronto Raptors, who are currently tied for the league leader in wins"})
# print(result)


import string
import copy
import gpt2

predictor = gpt2.Gpt2Predictor()

def predict_until_punctuation2(input_str2):
    if not input_str2:
        raise Exception('input string required')
    # print("third sent")
    output_str2 = copy.deepcopy(input_str2)

    while len(output_str2) != 0 and output_str2[-1] not in [".", "!", "?"] and output_str2[-2] not in [".", "!", "?"]:
        prediction2 = predictor.predict_json({"previous": output_str2})
        # print("p")
        # print(prediction2)
        # print("o")
        # print(output_str2)
        output_str2 += prediction2["words"][2]
        # print("o1")
        # print(output_str2)
    return output_str2
def predict_until_punctuation1(input_str1):
    if not input_str1:
        raise Exception('input string required')
    # print("SEcond sent")
    output_str1 = copy.deepcopy(input_str1)

    while len(output_str1) != 0 and output_str1[-1] not in [".", "!", "?"] and output_str1[-2] not in [".", "!", "?"]:
        prediction1 = predictor.predict_json({"previous": output_str1})
        # print("p")
        # print(prediction1)
        # print("o")
        # print(output_str1)
        output_str1 += prediction1["words"][1]
        # print("o1")
        # print(output_str1)
    return output_str1
def predict_until_punctuation(input_str):
  if not input_str:
    raise Exception('input string required')
  output_str = copy.deepcopy(input_str)
  while len(output_str) != 0 and output_str[-1] not in [".", "!", "?"] and output_str[-2] not in [".","!","?"]:
    prediction = predictor.predict_json({"previous": output_str})
    # print("p")
    # print(prediction)
    # print("o")
    # print(output_str)
    print(output_str)
    output_str += prediction["words"][1]
    # print("o1")
    # print(output_str)

  return output_str



if __name__ == "__main__":
    input_text=input()
    text = predict_until_punctuation(input_text)
    print(text)
    # if(text[-2]) in [".","!","?"]:
    #     print(text[0:-1])
    # else:
    #     print(text)
    # text1 = predict_until_punctuation1(input_text)
    # if (text1[-2]) in [".", "!", "?"]:
    #     print(text1[0:-1])
    # else:
    #     print(text1)
    # text2 = predict_until_punctuation2(input_text)
    # if (text2[-2]) in [".", "!", "?"]:
    #     print(text2[0:-1])
    # else:
    #     print(text2)